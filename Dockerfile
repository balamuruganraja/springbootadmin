FROM maven:3.5.2-jdk-8-alpine AS MAVEN_TOOL_CHAIN
COPY pom.xml /tmp/
COPY src /tmp/src/
WORKDIR /tmp/
RUN mvn -DskipTests=true package
RUN cd target && ls
EXPOSE 8762
ENTRYPOINT ["java","-jar","target/ServiceAdmin-0.0.1-SNAPSHOT.jar"]
